#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include <QtSql>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class Registration;
}

class Registration : public QDialog
{
    Q_OBJECT

public:
    explicit Registration(QWidget *parent = nullptr);
    ~Registration();

private slots:
    void on_buttonBox_accepted();

    void on_btnUPLOAD_clicked();

    void on_lnePNUM_cursorPositionChanged(int arg1, int arg2);

    void on_lneLN_textChanged(const QString &arg1);

private:
    Ui::Registration *ui;
    QSqlDatabase db = QSqlDatabase::database("db_conn");
};

#endif // REGISTRATION_H
