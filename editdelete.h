#ifndef EDITDELETE_H
#define EDITDELETE_H

#include <QDialog>
#include <QtSql>
#include <QMessageBox>
#include <QTableWidgetItem>

namespace Ui {
class EditDelete;
}

class EditDelete : public QDialog
{
    Q_OBJECT

public:
    explicit EditDelete(QWidget *parent = nullptr);
    ~EditDelete();

private slots:
    void loadInfo();
    void loadTable();
    void updateItem(QString, QString, QString, QString, QString, QString, QString, QString, QString, QString);
    void on_ledSearch_textChanged(const QString &arg1);

    void on_btnToggleEdit_clicked();

    void on_buttonBox_accepted();

    void on_btnDelete_clicked();

private:
    Ui::EditDelete *ui;
    QSqlDatabase db = QSqlDatabase::database("db_conn");

};

#endif // EDITDELETE_H
