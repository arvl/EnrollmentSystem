#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString bg_img = "/home/arvl/Public/QTEUNICE/2222.jpg";
    QPixmap amaLogo(bg_img);
    ui->lblAMAbg->setPixmap(bg_img);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnLogin_clicked()
{
    QString username = ui->ledUsername->text();
    QString password = ui->ledPassword->text();

    if (username=="admin" && password=="admin123"){

    mn = new MENU (this);
    mn -> show();
    ui -> ledUsername ->clear();
    ui -> ledPassword ->clear();
    }
}

void MainWindow::on_cbxShowPassword_stateChanged(int)
{
    if (ui->cbxShowPassword->checkState() == Qt::Checked) {
        ui->ledPassword->setEchoMode(QLineEdit::Normal);
    }
    else {
        ui->ledPassword->setEchoMode(QLineEdit::Password);
    }
}
