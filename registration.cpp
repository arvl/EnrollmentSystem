#include "registration.h"
#include "ui_registration.h"

Registration::Registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Registration)
{
    ui->setupUi(this);
     db.setDatabaseName("/home/arvl/EnrollmentSystem.db");
}

Registration::~Registration()
{
    delete ui;
}


void Registration::on_buttonBox_accepted()
{
    QString FN=ui->lneFN->text();
    QString LN=ui->lneLN->text();
    QString BD=ui->dteBC->text();
    QString GD=ui->cbxGENDER->currentText();
    QString PNUM=ui->lnePNUM->text();
    QString SD=ui->cbxSTRAND->currentText();
    QString GRYR=ui->lneGRYR->text();
    QString USN=ui->lneUSN->text();
    QString ADDR=ui->lneADD->text();
    QString PIC=ui->lineEdit->text();

   qDebug().noquote()<< "insert into STUDENTS values (" + USN + ",'" + FN + "','"+LN+"','"+BD+"','"+GD+"','"+PNUM+"','"+SD+"','"+GRYR+"','"+ADDR+"','"+PIC+"')";


    db.open();
    QSqlQuery successful_qry(db);

    if (successful_qry.exec("insert into STUDENTS values (" + USN + ",'" + FN + "','"+LN+"','"+BD+"','"+GD+"','"+PNUM+"','"+SD+"','"+GRYR+"','"+ADDR+"','"+PIC+"')")) {
        QMessageBox::information(this,"Registration Success","Your registration was successfully completed");
    }
    else{
        QMessageBox::critical(this,"Registration Failed","Your registration was not completed");

    }
    db.close();

}

void Registration::on_btnUPLOAD_clicked()
{
    QString picDir = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    ui->lineEdit->setText(QFileDialog::getOpenFileName(this, "Choose a cover image", picDir));

            if (!ui->lineEdit->text().isEmpty())

     {

            ui->lblUPLOAD->setPixmap(QPixmap(ui->lineEdit->text()).scaled(ui->lblUPLOAD->width(), ui->lblUPLOAD->height(), Qt::KeepAspectRatio));
            ui->lblUPLOAD->setEnabled(true);
            }
}

void Registration::on_lnePNUM_cursorPositionChanged(int arg1, int arg2 )
{

}

void Registration::on_lneLN_textChanged(const QString &arg1)
{

}
