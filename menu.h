#ifndef MENU_H
#define MENU_H

#include <QMainWindow>
#include <registration.h>
#include <editdelete.h>

namespace Ui {
class MENU;
}

class MENU : public QMainWindow
{
    Q_OBJECT

public:
    explicit MENU(QWidget *parent = nullptr);
    ~MENU();

private slots:


    void on_btnAddstudent_clicked();

    void on_btnEditstudent_clicked();

    void on_btnLogout_clicked();
signals:
    void displaytable();
    void loadTable ();
private:
    Ui::MENU *ui;
    Registration *rg;
    EditDelete *ed;

};

#endif // MENU_H
