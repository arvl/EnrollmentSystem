#include "menu.h"
#include "ui_menu.h"

MENU::MENU(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MENU)
{
    ui->setupUi(this);
    QPixmap marco_img("/home/arvl/Public/QTEUNICE/marco.jpg");
    ui->label->setPixmap(marco_img.scaled(ui->label->width(), ui->label->height()));
}

MENU::~MENU()
{
    delete ui;
}



void MENU::on_btnAddstudent_clicked()
{
    rg = new Registration(this);
    rg -> show ();
    rg ->setModal(true);
}

void MENU::on_btnEditstudent_clicked()
{
    ed = new EditDelete (this);
    ed -> show ();
    ed ->setModal(true);
    connect(this,SIGNAL(loadTable()),ed,SLOT(loadTable()));
    emit loadTable();
}

void MENU::on_btnLogout_clicked()
{
    this -> close();
}


