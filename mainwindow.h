#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <menu.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnLogin_clicked();

    void on_cbxShowPassword_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    MENU *mn;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "db_conn");

};
#endif // MAINWINDOW_H
