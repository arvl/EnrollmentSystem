#include "editdelete.h"
#include "ui_editdelete.h"

EditDelete::EditDelete(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDelete)
{
    ui->setupUi(this);
    db.setDatabaseName("/home/arvl/EnrollmentSystem.db");
    ui->tbvwSearch->horizontalHeader()->setStretchLastSection(true);
    connect(ui->tbvwSearch, SIGNAL(clicked(QModelIndex)), this, SLOT(loadInfo()));
}

EditDelete::~EditDelete()
{
    delete ui;
}


void EditDelete::loadInfo() {
    QModelIndex usn_index = ui->tbvwSearch->currentIndex();
    QString usn = usn_index.sibling(usn_index.row(),0).data().toString();

    if (db.open()) {
        QSqlQuery firstname_qry(db);
        firstname_qry.exec("select FIRSTNAME from STUDENTS where USN = " + usn);
        if (firstname_qry.next()) {
            QString firstname = firstname_qry.value(0).toString();
            ui->lneFN->setText(firstname);
        }

        QSqlQuery lastname_qry(db);
        lastname_qry.exec("select LASTNAME from STUDENTS where USN = " + usn);
        if (lastname_qry.next()) {
            QString lastname = lastname_qry.value(0).toString();
            ui->lneLN->setText(lastname);
        }
        QSqlQuery dteBC_qry(db);
        dteBC_qry.exec("select BIRTHDATE from STUDENTS where USN = " + usn);
        if (dteBC_qry.next()) {
            QString dteBC = dteBC_qry.value(0).toString();
            qDebug().noquote() << dteBC;

            QStringList dteBC_list = dteBC.split("/");

            QString months = dteBC_list[0];
            QString days = dteBC_list[1];
            QString years = dteBC_list[2];

            QString date_fmt;



            for (int i = 0; i < months.length(); i++) {
                date_fmt = date_fmt + "M";
            }

            date_fmt = date_fmt + "/";

            for (int i = 0; i < days.length(); i++) {
                date_fmt = date_fmt + "d";
            }

            date_fmt = date_fmt + "/";

            for (int i = 0; i < years.length(); i++) {
                date_fmt = date_fmt + "y";
            }

            qDebug().noquote() << date_fmt ;

            qDebug().noquote() << dteBC;



            ui->dteBC->setDate(QDate::fromString(dteBC, date_fmt));
        }
        QSqlQuery lnePNUM_qry(db);
        lnePNUM_qry.exec("select PHONENO from STUDENTS where USN = " + usn);
        if (lnePNUM_qry.next()) {
            QString lnePNUM = lnePNUM_qry.value(0).toString();
            qDebug().noquote() << lnePNUM;
            ui->lnePNUM->setText(lnePNUM);
         }

         ui->lneUSN->setText(usn);

         QSqlQuery lneGRYR_qry(db);
         lneGRYR_qry.exec("select GRADEYEAR from STUDENTS where USN = " + usn);
         if (lneGRYR_qry.next()) {
             QString lneGRYR = lneGRYR_qry.value(0).toString();
             qDebug().noquote() << lneGRYR;
             ui->lneGRYR->setText(lneGRYR);
         }

         QSqlQuery cbxGENDER_qry(db);
         cbxGENDER_qry.exec("select GENDER from STUDENTS where USN = " + usn);
         if (cbxGENDER_qry.next()) {
             QString cbxGENDER_str = cbxGENDER_qry.value(0).toString();
             qDebug().noquote() << cbxGENDER_str;
             ui->cbxGENDER->setCurrentText(cbxGENDER_str);
         }

         QSqlQuery cbxStrand_qry(db);
         cbxStrand_qry.exec("select STRAND from STUDENTS where USN = " + usn);
         if (cbxStrand_qry.next()) {
             QString cbxStrand_str = cbxStrand_qry.value(0).toString();
             qDebug().noquote() << cbxStrand_str;
             ui->cbxStrand->setCurrentText(cbxStrand_str);
         }

         QSqlQuery lneADD_qry(db);
         lneADD_qry.exec("select ADDRESS from STUDENTS where USN = " + usn);
         if (lneADD_qry.next()) {
             QString lneADD_str = lneADD_qry.value(0).toString();
             qDebug().noquote() << lneADD_str;
             ui->lneADD->setText(lneADD_str);
         }


         QSqlQuery lblPicture2x2_qry(db);
         lblPicture2x2_qry.exec("select PICTURE from STUDENTS where USN = " + usn);
         if (lblPicture2x2_qry.next()) {
             QString lblPicture2x2_str = lblPicture2x2_qry.value(0).toString();
             qDebug().noquote() << lblPicture2x2_str;
             ui->ledPicture2x2->setText(lblPicture2x2_str);
             if (ui->ledPicture2x2->text().isEmpty()) {
                 ui->lblPicture2x2->setText("No Image Data");
             }
             else {
                 QPixmap pic_2x2(lblPicture2x2_str);
                 ui->lblPicture2x2->setPixmap(pic_2x2.scaled(ui->lblPicture2x2->width(), ui->lblPicture2x2->height(), Qt::KeepAspectRatio));
             }
         }


         db.close();
    }
    else {
        qDebug() << db.lastError();
    }
}

void EditDelete::loadTable(){
    db.open();

    QSqlQueryModel *display_qrm = new QSqlQueryModel;
    display_qrm -> setQuery(db.exec("select * from STUDENTS"));
    ui->tbvwSearch->setModel(display_qrm);
    db.close();

    //int index_row = ui->tableView->selectionModel()->currentIndex().row();

}

void EditDelete::on_ledSearch_textChanged(const QString &USN)
{
    qDebug().noquote() << USN;

    db.open();

    QSqlQueryModel *display_qrm = new QSqlQueryModel;
    display_qrm -> setQuery(db.exec("select * from STUDENTS where USN like '" + USN + "%'"));
    ui->tbvwSearch->setModel(display_qrm);
    db.close();

}

void EditDelete::on_btnToggleEdit_clicked()
{
    if (ui->lneLN->isEnabled()) {
        ui->lneFN->setEnabled(false);
        ui->lneLN->setEnabled(false);
        ui->dteBC->setEnabled(false);
        ui->lnePNUM->setEnabled(false);
        ui->lneGRYR->setEnabled(false);
        ui->cbxGENDER->setEnabled(false);
        ui->cbxStrand->setEnabled(false);
        ui->lneADD->setEnabled(false);
        ui->pushButton->setEnabled(false);
        ui->btnDelete->setEnabled(false);
        ui->btnToggleEdit->setText("Enable Editing");
    }
    else {
        ui->lneFN->setEnabled(true);
        ui->lneLN->setEnabled(true);
        ui->dteBC->setEnabled(true);
        ui->lnePNUM->setEnabled(true);
        ui->lneGRYR->setEnabled(true);
        ui->cbxGENDER->setEnabled(true);
        ui->cbxStrand->setEnabled(true);
        ui->lneADD->setEnabled(true);
        ui->pushButton->setEnabled(true);
        ui->btnDelete->setEnabled(true);
        ui->btnToggleEdit->setText("Disable Editing");
    }
}

void EditDelete::updateItem(QString usn, QString first_name, QString last_name, QString birth_date, QString gender, QString phone_num, QString strand, QString grade_year, QString address, QString picture) {

    db.open();
    db.transaction();

    QSqlQuery first_name_qry(db),
            last_name_qry(db),
            birth_date_qry(db),
            gender_qry(db),
            phone_num_qry(db),
            strand_qry(db),
            grade_year_qry(db),
            address_qry(db),
            picture_qry(db);

    if (first_name_qry.exec("update STUDENTS set FIRSTNAME = \"" + first_name + "\" where USN = " + usn + "") &&
        last_name_qry.exec("update STUDENTS set LASTNAME = \"" + last_name + "\" where USN = " + usn + "") &&
        birth_date_qry.exec("update STUDENTS set BIRTHDATE = \"" + birth_date + "\" where USN = " + usn + "") &&
        gender_qry.exec("update STUDENTS set GENDER = \"" + gender + "\" where USN = " + usn + "") &&
        phone_num_qry.exec("update STUDENTS set PHONENO = \"" + phone_num + "\" where USN = " + usn + "") &&
        strand_qry.exec("update STUDENTS set STRAND = \"" + strand + "\" where USN = " + usn + "") &&
        grade_year_qry.exec("update STUDENTS set GRADEYEAR = \"" + grade_year + "\" where USN = " + usn + "") &&
        address_qry.exec("update STUDENTS set ADDRESS = \"" + address + "\" where USN = " + usn + "") &&
        picture_qry.exec("update STUDENTS set PICTURE = \"" + picture + "\" where USN = " + usn + "") ) {



        QMessageBox::information(this, "Update Success", "Update table complete!");
        db.commit();
    }
    else {
        qDebug().noquote() << first_name_qry.lastError();
        qDebug().noquote() << last_name_qry.lastError();
        qDebug().noquote() << birth_date_qry.lastError();
        qDebug().noquote() << gender_qry.lastError();
        qDebug().noquote() << phone_num_qry.lastError();
        qDebug().noquote() << strand_qry.lastError();
        qDebug().noquote() << grade_year_qry.lastError();
        qDebug().noquote() << address_qry.lastError();
        qDebug().noquote() << picture_qry.lastError();

        QMessageBox::warning(this, "Update Error", "Update table failed!");
        db.rollback();
    }

    db.close();

}

void EditDelete::on_buttonBox_accepted()
{
    QString usn = ui->lneUSN->text();
    QString first_name = ui->lneFN->text();
    QString last_name = ui->lneLN->text();
    QString birth_date = ui->dteBC->text();
    QString gender = ui->cbxGENDER->currentText();
    QString phone_num = ui->lnePNUM->text();
    QString strand = ui->cbxStrand->currentText();
    QString grade_year = ui->lneGRYR->text();
    QString address = ui->lneADD->text();
    QString picture = ui->ledPicture2x2->text();

    updateItem(usn, first_name, last_name, birth_date, gender, phone_num, strand, grade_year, address, picture);
}

void EditDelete::on_btnDelete_clicked()
{
    QString usn = ui->lneUSN->text();
    db.open();
    QSqlQuery delete_qry(db);
    QMessageBox::StandardButton deletePrompt = QMessageBox::question(this, "Delete Item", "The current entry will be deleted. Continue?", QMessageBox::Yes|QMessageBox::No);

    if (deletePrompt == QMessageBox::Yes) {
        if (delete_qry.exec("delete from STUDENTS where USN = " + usn ) ) {
            QMessageBox::information(this, "Delete Item", "Delete item success!");
            accept();
        }
        else {
            QMessageBox::warning(this, "Delete Item", "Delete item failed!");
        }
    }
    db.close();

}
